Exploration of Weakly/Semi-Supervised Segmentation.
========================================================================================

**THIS IS WORK IN PROGRESS!**

Exploration of Weakly/Semi-Supervised Segmentation (wssseg).

This README covers the technical aspects.
See `ARTICLE.md` for the actual discussion on Weakly/Semi-Supervised Segmentation.


Getting Started / Setup the Project
========================================================================================

Create the conda environment:

```bash
make env_create
conda activate wssseg
pre-commit install
```

Test the project:
```bash
# Run tests
pytest
# Rerun tests on file change
pytest-watch

# Run all pre-commit checks (without commiting anything)
pre-commit run
```


Setup the Project
========================================================================================
```bash
make data
```


Tools
========================================================================================
## Tool: `mkexperiment`
Create experiments with the CLI tool `mkexperiment`.


## Tool: `mknotebook`
Create notebook with the CLI tool `mknotebook`.


## Tool: `pre-commit`
We use [pre-commit](https://pre-commit.com/) to automatically run basic checks
before every commit.

You can run the checks also manually:
```bash
pre-commit run
pre-commit run --all-files
```

(You can bypass the checks with `git commit -n`, but don't do it, fix the
errors instead!)


Code Style
========================================================================================
We follow
[PEP8](https://pep8.org/),
use [black](https://github.com/python/black),
[isort](https://pypi.org/project/isort/),
and [flake8](https://pypi.org/project/flake8/).
[`pre-commit`](https://pypi.org/project/flake8/) should take care of most problems.


Project Organization
========================================================================================
```
    ├── LICENSE
    ├── Makefile           <- Makefile with commands like `make data` or `make train`
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── experiments        <- All experiments you run.
    │   │                     Naming conventions:
    │   │                         <YYYY-MM-DD>_<INITIALS>_<DESCRIPTION>.py
    │   ├── WIP            <- Define your experiments here and move them once ready
    │   └── <project>      <- Group experiments by projects
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks are meant to be lab notebboks.
    │                         Refactor useful code and put it into wssseg
    │                         Naming convention:
    │                             <YYYY-MM-DD>_<INITIALS>_<DESCRIPTION>.ipynb
    │                             2018-06-01_SO_initial_exlorative_data_analysis.ipynb
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── setup.py           <- makes project pip installable (pip install -e .)
    ├── wssseg             <- Source code for use in this project.
    │   └── torchseg       <- TODO

```
