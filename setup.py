from setuptools import find_packages, setup


setup(
    name="wssseg",
    packages=find_packages(),
    version="0.0.1",
    description="Exploration of Weakly/Semi-Supervised Segmentation",
    author="Stefan Otte",
    license="",
    install_requires=[],
    entry_points={
        "console_scripts": [
            "mkexperiment=wssseg.cli.mkexperiment:main",
            "mknotebook=wssseg.cli.mknotebook:main",
        ]
    },
)
