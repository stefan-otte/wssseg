Experiments with Weakly / Semi-supervised Segmentation
========================================================================================

This weekend, I decided to dabble with Weakly / Semi-supervised Segmentation,
mostly because I haven't really done too much with it,
but at the same time I really value active learning, transfer learning, self-supervised learning...pretty much everything that reduced the need for labeled data.

I also decided that this would be an opportunity to investigate PyTorch's
segmentation support features that were added in
[torchvision 0.3](https://pytorch.org/blog/torchvision03/).


Literature Review / Related Work
----------------------------------------------------------------------------------------
### Terms
In *semi-supervisId learning* we have a small labeled dataset and a bigger unlabeled datasest.
We make assumptions about the structure of the unlabeled data to extract information.

In *weakly-supervised learning* we have weaker labels,
e.g. only image level labels (dog/cat),
instead of the finer labels we're interested in, e.g. pixel wise label (segmentation of dog/cat).

In practice these two approaches overlap quite a bit.
Aspects of *transfer learning*, *self-supervised*, and *few-shot learning* appear.

See https://hazyresearch.github.io/snorkel/blog/ws_blog_post.html for more detailed discussion on the above terms.


### Seed, Expand and Constrain: Three Principles for Weakly-Supervised Image Segmentation | 2016
https://arxiv.org/abs/1603.06098 |
[references/1603.06098.pdf](references/1603.06098.pdf)

The basic idea of this paper for weakly-supervised segmentation is
> to seed with weak localization cues, to expand objects
> based on the information about which classes can occur in an image, and
> to constrain the segmentations to coincide with object boundaries.

They introduce a new loss
```
new loss = seed loss + expansion loss + contrain to boundary loss
```
where
- the seed loss uses the object location cues generated by a classification network,
- the expansion loss is responsible to expand the region of the proposed regions,
- the constrain to boundary loss improves boundaries ob the segmentation.

The architecture looks like this:

![sec arch](resources/imgs/seed_expand_constrain_arch.png)

There are a few moving peaces:
a classification network for weak localization (think CAM-like approaches),
the actual segmentation network,
a CRF that is used during training as part of the boundary loss to improve the boundaries (the CRF is also used at test time to improve the segmentation),
and the newly defined loss function.

This paper is quite typical for weakly-supervised segmentation:
a classification network is used to seed segmentation regions,
these regions have to be expanded and constraint,
and as consequences of the above, there are many moving parts,


### Decoupled Deep Neural Network for Semi-supervised Semantic Segmentation | 2015
https://papers.nips.cc/paper/5858-decoupled-deep-neural-network-for-semi-supervised-semantic-segmentation |
[references/5858-decoupled-deep-neural-network-for-semi-supervised-semantic-segmentation.pdf] (references/5858-decoupled-deep-neural-network-for-semi-supervised-semantic-segmentation.pdf)

In this paper they decouple classification and segmentation.
First a classification network learns to classify the objects (with the weak labels).
Then they perform binary segmentation in the segmentation network using the learned classification network as backbone.
No post-processing for the segmentation map is required.

This is the architecture:
![arch](resources/imgs/decoupled_dnn_arch.png)

Note that the "bridging layers" forward earlier activations from the classification network.

Even though two separate networks are used the overall setup in this paper is very straight forward.
Today, this procedure would be summarized as pretraining a classification network, then adding a segmentation head while using some intermediate layers of the classification backbone.


### Simple Does It: Weakly Supervised Instance and Semantic Segmentation | 2016
https://arxiv.org/abs/1603.07485 |
[references/1603.07485.pdf](references/1603.07485.pdf)

In this paper they use weak annotations in the form of *object bounding boxes* to do semantic and instance segmentation.

They describe the main idea:
> We view the problem of weak supervision as an issue of input label noise. We explore recursive training as a de-noising strategy, where convnet predictions of the pre- vious training round are used as supervision for the next round. We also show that, when properly used, “classic computer vision” techniques for box-guided instance seg- mentation are a source of surprisingly effective supervision for convnet training.

I.e. the segmentation prediction is used as ground truth (unfiltered (*Naive*) or after applying post-processing steps (*Box* and *Box^i*)).
Alternatively, GrabCut-like methods are used to turn the bounding boxes into "proper" segmentations.
> We think of this as “old school meets new school”: we use the noisy outputs of classic computer vision methods, box-driven figure-ground segmentation [36] and object proposal [35] techniques, to feed the training of a convnet.

Note that the training step does not have to be modified compared to the fully supervised case.
![simple does it idea](resources/imgs/simple_does_it.png)

We would have expected that the recursive training would crash after a few iterations, but the post-processing (Box and Box^1) seems to work quite well.
Regarding the use of GrabCut-like methods to generate segmentation targets feels a bit hacky.
That being said, why not use the signal that classic CV methods can generate.
All in all we like the simplicity of the approach.


### FickleNet: Weakly and Semi-supervised Semantic Image Segmentation using Stochastic Inference | 2019
https://arxiv.org/abs/1902.10421 |
[references/1902.10421.pdf](resources/1902.10421.pdf)

This is an interesting paper.
Like so many papers, including "Seed, Expand and Constrain",
they somehow have to extract pixel-level information from the image level annotation.

In general, the localization maps from classification networks focus on the discriminative parts of the image, but not the whole class, i.e. localization maps don't segment the whole object.
Where "Seed, Expand and Constrain" explicitly expanded and constraint regions (with losses, special pooling layers, and the use of CRFs)
this paper creates different localization maps for an object with an dropout-like approach (they call it "stochastic selection of hidden units").
These different maps are then put together and capture more of the object.
(Note that they still use a loss that contains terms for seeding and boundaries.)

> FickleNet explores diverse combinations of locations on feature maps created by generic deep neural networks. It selects hidden units randomly and then uses them to obtain activation scores for image classification. FickleNet implicitly learns the coherence of each location in the feature maps, resulting in a localization map which identifies both discriminative and other parts of objects

Note that the implementation of the dropout-like approach is quite interesting.

![ficklenet](resources/imgs/fickle_net.png)

The training procedure look like this:
> FickleNet, which uses stochastic selection of hidden units, is trained for multi-class classification. It then generates localization maps of training images. Finally, the localization maps are used as pseudo-labels to train a segmentation network.


### Universal Semi-Supervised Semantic Segmentation | 2019
https://arxiv.org/abs/1811.10323 |
[references/1811.10323.pdf](references/1811.10323.pdf)

The previous papers tried to explicitly extract pixel-level information from image-level labels (using activation maps, expanding regions, limiting the boundaries, classic CV methods, more advanced methods to create activation maps, etc.).
This paper is a bit different...I would say much cooler, but I'm clearly biased:
I had great success with
[content-based image retrieval methods that were trained on auxiliary task in different domains](https://nodata.science/similarity-search-with-neural-networks.html)
and I'm a fan of [LeCun's cake](https://miro.medium.com/max/4416/1*bvMhd_xpVxfJYoKXYp5hug.png).

They train a *single* segmentation model for different domains (common backbone, domain specific decoder with domain specific labels).
This is a classic supervised task.

Then they add an additional head for unsupervised learning with the goal to

> to align pixel level deep feature representations from multiple domains using entropy regularization based objective functions. Entropy regularization uses unsupervised examples and helps in encouraging low density separation between the feature representations and improve the confidence of predictions.

The overall architecture look like this:
![usssss](resources/imgs/universal_ssss_2.png)

![usssss](resources/imgs/universal_ssss.png)

All in all this is a very nice methods to use knowledge from different domains and handle the shortage of pixel-wise labels.
The overall approach is quite clean except maybe "B.1. Calculating the label embeddings".


#### Summary
The biggest challenge is to extract localization information from the weak image-level annotations, and most approaches feel a bit inelegant.
That's why I really appreciate the simple and general approach of
"Universal Semi-Supervised Semantic Segmentation".


Dataset
----------------------------------------------------------------------------------------

For this experiment we're going to use the
["2D Semantic Labeling - Vaihingen"](http://www2.isprs.org/commissions/comm3/wg4/2d-sem-label-vaihingen.html) dataset

> The data set contains 33 patches (of different sizes), each consisting of a true orthophoto (TOP) extracted from a larger TOP mosaic, see Figure below and a DSM. For further information about the original input data, please refer to the data description of the object detection and 3d reconstruction benchmark.

![overview](resources/imgs/overview_tiles.jpg "Tile overview of dataset")

We'll only consider the TOP, not the DSM data.

The size of the images varies, but is around 2k x 2k.
There are 6 classes in this dataset:
```
# NAME             COLOR      ID
("impervious",     (1, 1, 1), 0),    # WHITE
("building",       (0, 0, 1), 1),    # BLUE
("low_vegetation", (0, 1, 1), 2),    # TURQUOISE
("tree",           (0, 1, 0), 3),    # GREEN
("car",            (1, 1, 0), 4),    # YELLOW
("clutter",        (1, 0, 0), 255),  # RED
```

We divided the dataset into subsets
- n1: 3 randomly selected patches.
- n2: 23 randomly selected patches.
- n2_weak: the same patches as n2 but only weak labels (which class does exist in the given patch) are provided, no segmentation mask.
- nval: 7 randomly selected patches for evaluation.


### Data and Preprocessing
You have to
[register](http://www2.isprs.org/commissions/comm3/wg4/data-request-form2.html)
to get a username/password to download the data.
The username/password must be available as env variable:
```bash
export ISPRS_USER "foo"
export ISPRS_PW "bar"
```
To download and preproces the data call `make data`.
Check out `data/raw/` and `data/interim/`.


Technicalities
----------------------------------------------------------------------------------------
As mentioned earlier, we're using the new torchvision segmentation functionality,
and more specifically their reference implementation from
https://github.com/pytorch/vision/tree/master/references/segmentation.

We adjusted the implementation to our requirements.
The code is available under `wssseg/torchseg/`.
Here is a brief overview:
- `wssseg/torchseg/transforms.py` contains transforms that transform image and mask at the same time.
- `wssseg/torchseg/utils.py` contains random helpers, loggers, and metrics.
- `wssseg/cli/train_seg.py` is the CLI entrypoint that runs a training.
  Run `python wssseg/cli/train_seg.py --help` to see all the options.


We're *tracking the exeperiments* with neptune.ml.
You can see all experiments here: https://ui.neptune.ml/sotte/wssseg/experiments.
The changes for neptune to the original `train.py` are pretty minimal.
Look for:
```python
_log_metric_logger(metric_logger, epoch, prefix="valid")
_log_confmat(confmat, epoch)
```
and just Ctrl+F `neptune`.
It should be self-explanatory.


Training Model
----------------------------------------------------------------------------------------
### Training details:
- We trained for only 10 epochs.
- We used the Adam optimizer.
- The learning rate varied but was generally around 0.01 (see neptune for details).
- A [FCN ResNet101](https://pytorch.org/docs/stable/torchvision/models.html#torchvision.models.segmentation.fcn_resnet101)
  was used in all experiments

We're going to use the `global_correct` metric,
i.e. the fraction of correctly classified pixels,
during this discussion.

### Simple Segmentation Models
If we train on the bigger n2 dataset we can easily classify >86% of the pixels.
This tells us the upper limit of how good we can get.
See https://ui.neptune.ml/sotte/wssseg/compare?shortId=%5B%22WSSSEG-20%22%2C%22WSSSEG-19%22%5D.

Simple segmentation baseline trained on N1 yields ~80%.
See https://ui.neptune.ml/sotte/wssseg/e/WSSSEG-18/charts.

Same setup, but when using a backbone pretrained on imagenet data we achieved ~82%.
Note that the model trained slower/took a few epochs to get this good.
See https://ui.neptune.ml/sotte/wssseg/e/WSSSEG-21/charts.

Same setup, but the backbone was frozen, we achived only achived ~72% and the model trained slower.
See https://ui.neptune.ml/sotte/wssseg/e/WSSSEG-24/charts.

Here are randomly selected predictions:

Sample WSSEG-18:
![Sample WSSEG-18](resources/imgs/sample_segmentations/WSSSEG-18_top_mosaic_09cm_area10_x0_y224.png)

Sample WSSEG-20:
![Sample WSSEG-20](resources/imgs/sample_segmentations/WSSSEG-20_top_mosaic_09cm_area10_x0_y224.png)

Sample WSSEG-21:
![Sample WSSEG-21](resources/imgs/sample_segmentations/WSSSEG-21_top_mosaic_09cm_area10_x0_y224.png)


### Pretraining classification network on `n2_weak`
Most of the weakly supervised segmentation models seem kinda finicky.
We decided to follow the simplest approach similar to "Decoupled Deep Neural Network for Semi-supervised Semantic Segmentation".

We trained a network on the weak labels,
i.e. we trained a classification network on the multi-label classification task.
For the implementation see `train_classifier.py` and for result see
https://ui.neptune.ml/sotte/wssseg/experiments?tag=%5B%22weakly_pretrained%22%5D.
The network was the backbone of the FCN ResNet101 (a slightly modified ResNet101)
plus a very simple classification head.

We then used classification backbone as backbone for the segmentation network.
and trained with and without freezing the backbone.
See https://ui.neptune.ml/sotte/wssseg/experiments?tag=%5B%22weakly_pretrained%22%5D
for all results for networks pretrained on `n2_weak`.

The results were pretty underwhelming.
We achived up to <75% with a not not frozen backbone.
See https://ui.neptune.ml/sotte/wssseg/e/WSSSEG-33/charts.

This indicates that the
Note that the patch-level labels are not that informative.
The two majority classes make up >60% of the pixels

Sample WSSEG-33:
![Sample WSSEG-33](resources/imgs/sample_segmentations/WSSSEG-33_top_mosaic_09cm_area10_x0_y224.png)


### Weak/Semi-supervised segmentation
TODO


Conclusion
----------------------------------------------------------------------------------------
TODO


Misc
----------------------------------------------------------------------------------------
During this project we got some random crashes.
Normally, PyTorch is rock solid, but in this project it really slowed down my progress.
```
Traceback (most recent call last):
  File "wssseg/torchseg/train.py", line 344, in <module>
    main(args)
  File "wssseg/torchseg/train.py", line 260, in main
    epoch=epoch,
  File "wssseg/torchseg/train.py", line 85, in evaluate
    confmat.update(target.flatten(), output.argmax(1).flatten())
  File "/home/stefan/projects/wssseg/wssseg/torchseg/utils.py", line 87, in update
    inds = n * a[k].to(torch.int64) + b[k]
RuntimeError: copy_if failed to synchronize: cudaErrorLaunchFailure: unspecified launch failure
```

```
...
fish: “python wssseg/torchseg/train.py…” terminated by signal SIGSEGV (Address boundary error)
```
