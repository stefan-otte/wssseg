from typing import Sequence

import matplotlib.pyplot as plt
from PIL import Image
from torchvision.datasets import VisionDataset

from wssseg.utils import get_project_root


########################################################################################
# Run this file to get this list
n1 = [
    "top_mosaic_09cm_area8.tif",
    "top_mosaic_09cm_area27.tif",
    "top_mosaic_09cm_area28.tif",
]

n2 = [
    "top_mosaic_09cm_area12.tif",
    "top_mosaic_09cm_area6.tif",
    "top_mosaic_09cm_area4.tif",
    "top_mosaic_09cm_area7.tif",
    "top_mosaic_09cm_area1.tif",
    "top_mosaic_09cm_area13.tif",
    "top_mosaic_09cm_area22.tif",
    "top_mosaic_09cm_area20.tif",
    "top_mosaic_09cm_area26.tif",
    "top_mosaic_09cm_area38.tif",
    "top_mosaic_09cm_area16.tif",
    "top_mosaic_09cm_area17.tif",
    "top_mosaic_09cm_area11.tif",
    "top_mosaic_09cm_area3.tif",
    "top_mosaic_09cm_area5.tif",
    "top_mosaic_09cm_area23.tif",
    "top_mosaic_09cm_area14.tif",
    "top_mosaic_09cm_area29.tif",
    "top_mosaic_09cm_area30.tif",
    "top_mosaic_09cm_area24.tif",
    "top_mosaic_09cm_area2.tif",
    "top_mosaic_09cm_area31.tif",
    "top_mosaic_09cm_area37.tif",
]

nval = [
    "top_mosaic_09cm_area33.tif",
    "top_mosaic_09cm_area34.tif",
    "top_mosaic_09cm_area35.tif",
    "top_mosaic_09cm_area21.tif",
    "top_mosaic_09cm_area15.tif",
    "top_mosaic_09cm_area32.tif",
    "top_mosaic_09cm_area10.tif",
]


########################################################################################
class VaihingenSegmentationDataset(VisionDataset):
    def __init__(self, images: Sequence, masks: Sequence, transforms=None):
        assert len(images) == len(masks)
        _image_names = set([p.name.split("_")[-1] for p in images])
        _mask_names = set([p.name.split("_")[-1] for p in masks])
        assert _image_names == _mask_names

        super().__init__(root=None, transforms=transforms)
        self.images = images
        self.masks = masks

    def __len__(self):
        return len(self.images)

    def __getitem__(self, index):
        img = Image.open(self.images[index]).convert("RGB")
        target = Image.open(self.masks[index])

        if self.transforms is not None:
            img, target = self.transforms(img, target)

        return img, target

    def show_sample(self, idx, size=500, figsize=(20, 6)):
        size = size, size
        img = Image.open(self.images[idx]).resize(size)
        mask = Image.open(self.masks[idx]).resize(size)

        fig, axes = plt.subplots(1, 3, figsize=figsize)
        axes[0].imshow(img)
        axes[1].imshow(mask)
        axes[2].imshow(img)
        axes[2].imshow(mask, alpha=0.3)

        fig.suptitle(f"Sample: {idx}")
        for ax in axes:
            ax.axis("off")


if __name__ == "__main__":
    import torch

    torch.manual_seed(0)

    images = (get_project_root() / "data/raw/top").ls
    index = torch.randperm(len(images))
    n1_index, n2_index, nval_index = torch.split(index, (3, 23, 7))

    def select(seq, index):
        return [seq[i].name for i in index]

    n1 = select(images, n1_index)
    print("n1 =", n1)
    print()

    n2 = select(images, n2_index)
    print("n2 = ", n2)
    print()

    nval = select(images, nval_index)
    print("nval =", nval)
