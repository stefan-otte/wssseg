import json
import os
from contextlib import contextmanager
from typing import Dict, Sequence

import neptune
from devtools import debug

from wssseg.utils import get_project_root


@contextmanager
def track_exp(tags: Sequence[str], params: Dict):
    """Context manager that tracks experiments with neptune.

    Yield the output_dir (type Path).
    """
    assert "NEPTUNE_API_TOKEN" in os.environ
    neptune_namespace = "sotte/wssseg"
    neptune.init(neptune_namespace)
    with neptune.create_experiment(params=params) as exp:
        output_dir = get_project_root() / "models" / neptune_namespace / exp._id
        output_dir.mkdir(parents=True)
        with (output_dir / "params.json").open("w") as f:
            f.write(json.dumps(params))

        neptune.set_property("output_dir", str(output_dir))
        neptune.append_tags(*tags)
        debug(params, tags)

        yield output_dir
