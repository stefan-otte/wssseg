#!/usr/bin/env python
# -*- coding: utf-8 -*-
import datetime
import getpass
from pathlib import Path

import click
from jinja2 import Template


def get_project(src: str) -> str:
    """Extract the project from `src` or user input."""
    if src.startswith("experiments"):
        project = Path(src).parts[1]
        print(project)
    else:
        projects = sorted([x.name for x in Path("experiments").iterdir() if x.is_dir()])
        msg = [f" ({i}) '{p}'" for i, p in enumerate(projects, start=1)]
        while True:
            try:
                print("Select the project:")
                print("\n".join(msg))
                prj_id = click.prompt("Enter project number", type=int)
                if prj_id < 1:
                    raise IndexError()
                project = projects[prj_id - 1]
                break
            except IndexError:
                print()
                click.secho(
                    f"Project number must be between 1 and {len(projects)}", fg="red"
                )
    return project


@click.command()
@click.option(
    "--src",
    help="The experiment template.",
    default="resources/experiment_template.py",
    show_default=True,
    type=click.Path(file_okay=True, dir_okay=False, exists=True),
)
@click.option("--description", help="Description for the experiment", prompt=True)
def main(src, description):
    """
    Create a new experiment for your user (from src).

    The new experiment is stored here:

        experiments/<project>/<YYYY-MM-DD>_<USER>_<DESCRIPTION>/main.py

    \b
    Examples:
    > # Create a new experiment from the default template
    > mkexperiment
    > # Create a new experiment from an old experiment
    > mkexperiment --src=experiments/FOO/my_exp/main.py
    """
    project = get_project(src)
    desc_underscore = description.lower().strip().replace(" ", "_")
    now = datetime.datetime.now().strftime("%Y-%m-%d")
    user = getpass.getuser().upper()
    dst = Path("experiments") / project / f"{now}_{user}_{desc_underscore}" / "main.py"
    print("Creating experiment:")
    print(f"  {src} --> {dst}")

    dst.parent.mkdir(parents=True, exist_ok=True)
    with open(src, "r") as f:
        rendered = Template(f.read()).render(title=description, date=now)
    with open(dst, "w") as f:
        f.write(rendered)


if __name__ == "__main__":
    main()
