#!/usr/bin/env python
# coding: utf-8
import time
from pathlib import Path

import torch
import torch.nn as nn
import torch.nn.functional as F
from torchvision.models.segmentation import fcn_resnet101

import wssseg.torchseg.transforms as T
from wssseg.datasets import VaihingenSegmentationDataset
from wssseg.experiment import track_exp
from wssseg.torch_utils import DEVICE, to_multi_hot
from wssseg.torchseg.train import get_transform
from wssseg.torchseg.utils import MetricLogger, SmoothedValue
from wssseg.utils import get_project_root

Path.ls = property(lambda path: sorted(list(path.iterdir())))


########################################################################################
# MODEL
class Classifier(nn.Module):
    def __init__(self, backbone: nn.Module, head: nn.Module) -> None:
        super().__init__()
        self.backbone = backbone
        self.head = head

    def forward(self, x):
        x = self.backbone(x)
        # it's a dict because we use the segmentation backbone
        x = self.head(x["out"])
        return x


def get_model(n_classes: int = 5):
    base_model = fcn_resnet101()
    head = nn.Sequential(
        nn.AdaptiveMaxPool2d(1),
        nn.Flatten(-3),  # 2048
        nn.Linear(2048, 128),
        nn.ReLU(inplace=True),
        nn.Linear(128, n_classes),
    )
    return Classifier(backbone=base_model.backbone, head=head)


########################################################################################
# DATA
class Mask2MultiLabel:
    """Transform that turns a segmentation mask into a a multi hot encoded vector."""

    def __init__(self, n_classes: int) -> None:
        self.n_classes = n_classes

    def __call__(self, image, target):
        target = to_multi_hot(
            torch.unique(target).unsqueeze(0), self.n_classes
        ).squeeze(0)
        return image, target


def get_dataset(path: str, transforms):
    root = get_project_root()
    ds = VaihingenSegmentationDataset(
        (root / f"{path}_images").ls, (root / f"{path}_masks").ls, transforms
    )
    return ds


def get_data(batch_size, n_classes=5):
    # Note that we turn the normal segmentation dataset into a classification dataset
    # by using the Mask2MultiLabel transform.
    train = True
    train_ds = get_dataset(
        "data/interim/n2",
        T.Compose([get_transform(train=train), Mask2MultiLabel(n_classes)]),
    )
    assert train_ds[0][1].shape == (n_classes,)
    train_dl = torch.utils.data.DataLoader(
        train_ds, batch_size=batch_size, shuffle=train, num_workers=4  # args.workers,
    )

    train = False
    valid_ds = get_dataset(
        "data/interim/nval",
        T.Compose([get_transform(train=train), Mask2MultiLabel(n_classes)]),
    )
    assert valid_ds[0][1].shape == (n_classes,)
    valid_dl = torch.utils.data.DataLoader(
        valid_ds, batch_size=batch_size, shuffle=train, num_workers=4  # args.workers,
    )

    batch = next(iter(train_dl))
    assert batch[0].shape == (batch_size, 3, 224, 224)
    assert batch[1].shape == (batch_size, n_classes), batch[1].shape

    batch = next(iter(valid_dl))
    assert batch[0].shape == (batch_size, 3, 224, 224)
    assert batch[1].shape == (batch_size, n_classes), batch[1].shape

    return train_dl, valid_dl


########################################################################################
# TRAIN & EVAL
def train_one_epoch(model, loss_fn, optimizer, data_loader, device, epoch, print_freq):
    model.train()
    metric_logger = MetricLogger(delimiter="  ")
    metric_logger.add_meter("lr", SmoothedValue(window_size=1, fmt="{value}"))
    metric_logger.add_meter("img/s", SmoothedValue(window_size=10, fmt="{value}"))

    header = "Epoch: [{}]".format(epoch)
    for image, target in metric_logger.log_every(data_loader, print_freq, header):
        start_time = time.time()
        image, target = image.to(device), target.to(device)
        output = model(image)
        loss = loss_fn(output, target)

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        # Logging...
        batch_size = image.shape[0]
        target, output = target.cpu(), output.cpu()
        metric_logger.update(loss=loss.item(), lr=optimizer.param_groups[0]["lr"])
        acc_total, acc_per_class = acc_dummy(output, target)
        metric_logger.meters["acc_total"].update(acc_total.item(), n=batch_size)
        for i, _acc in enumerate(acc_per_class):
            metric_logger.meters[f"acc_cls_{i}"].update(_acc.item(), n=batch_size)
        metric_logger.meters["img/s"].update(batch_size / (time.time() - start_time))

    return metric_logger


def evaluate(model, loss_fn, data_loader, device):
    model.eval()
    metric_logger = MetricLogger(delimiter="  ")
    header = "Test:"
    with torch.no_grad():
        for image, target in metric_logger.log_every(data_loader, 1, header):
            image = image.to(device, non_blocking=True)
            target = target.to(device, non_blocking=True)
            output = model(image)
            loss = loss_fn(output, target)

            # Logging...
            batch_size = image.shape[0]
            target, output = target.cpu(), output.cpu()
            metric_logger.update(loss=loss.item())
            acc_total, acc_per_class = acc_dummy(output, target)
            metric_logger.meters["acc_total"].update(acc_total.item(), n=batch_size)
            for i, _acc in enumerate(acc_per_class):
                metric_logger.meters[f"acc_cls_{i}"].update(_acc.item(), n=batch_size)


def acc_dummy(pred: torch.Tensor, target: torch.Tensor) -> torch.Tensor:
    """This is not a proper multi-label metric but maybe good enough to indicate learning.

    Examples:
        >>> acc_dummy(
        ...     torch.tensor([
        ...         [0.1, 0.3, 0.9],
        ...         [0.1, 0.7, 0.9],
        ...     ]),
        ...     torch.tensor([
        ...         [0., 0., 1.],
        ...         [0., 1., 1.],
        ...     ])
        ... )
        (tensor(1.), tensor([1., 1., 1.]))
        >>> acc_dummy(
        ...     torch.tensor([
        ...         [0.1, 0.3, 0.9],
        ...         [0.1, 0.7, 0.9],
        ...     ]),
        ...     torch.tensor([
        ...         [1., 1., 0.],
        ...         [1., 0., 0.],
        ...     ])
        ... )
        (tensor(0.), tensor([0., 0., 0.]))
        >>> acc_dummy(
        ...     torch.tensor([
        ...         [0.1, 0.3, 0.9],
        ...         [0.1, 0.7, 0.9],
        ...     ]),
        ...     torch.tensor([
        ...         [1., 1., 0.],
        ...         [0., 1., 1.],
        ...     ])
        ... )
        (tensor(0.5000), tensor([0.5000, 0.5000, 0.5000]))
    """
    pred, target = pred.cpu(), target.cpu()
    pred_classes = pred > 0.5
    return (
        (pred_classes == target).float().mean(),
        (pred_classes == target).float().mean(dim=0),
    )


def overfit_one_batch(dataloader, model, loss_fn, optimizer, device, steps: int = 20):
    model.to(device)
    x, y = next(iter(dataloader))
    x, y = x.to(device), y.to(device)
    for i in range(1, steps + 1):
        pred = model(x)
        loss = loss_fn(pred, y)

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        print(i, loss.cpu().item())


########################################################################################
def main(epochs: int = 4, batch_size: int = 16):
    params = locals()
    train_dl, valid_dl = get_data(batch_size)
    model = get_model()
    # multi label classification -> don't use simple cross entropy!
    loss_fn = F.binary_cross_entropy_with_logits
    optimizer = torch.optim.Adam(model.parameters(), 0.001)

    tags = ["WIP", "N2_WEAK"]
    with track_exp(tags=tags, params=params) as output_dir:
        model.to(DEVICE)
        for epoch in range(1, epochs + 1):
            train_one_epoch(
                model=model,
                loss_fn=loss_fn,
                optimizer=optimizer,
                data_loader=train_dl,
                device=DEVICE,
                epoch=epoch,
                print_freq=5,
            )
            evaluate(model, loss_fn, valid_dl, DEVICE)
            torch.save(
                {"model": model.state_dict()}, str(output_dir / f"model_{epoch}.pth")
            )


if __name__ == "__main__":
    main()
