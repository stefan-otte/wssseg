#!/usr/bin/env python
# -*- coding: utf-8 -*-
import datetime
import getpass
from pathlib import Path

import click
from jinja2 import Template


@click.command()
@click.option(
    "--src",
    help="The notebook to clone.",
    default="resources/notebook_template.ipynb",
    show_default=True,
    type=click.Path(file_okay=True, dir_okay=False, exists=True),
)
@click.option("--description", prompt=True)
def main(src, description):
    """
    Create a new notebook for your user (from src).

    The name of the notebook has the following structure:

        notebooks/<YYYY-MM-DD>_<USER>_<DESCRIPTION>.ipynb

    \b
    Examples:
    > # Just create a new NB. You're prompted for a description.
    > mknotebook Old Idea
    > # Specify the description directly
    > mknotebook --description "EDA v2"
    > # Specify the src notebook yourself.
    > mknotebook --src notebooks/old_idea.ipynb
    """
    desc_underscore = description.lower().strip().replace(" ", "_")
    now = datetime.datetime.now().strftime("%Y-%m-%d")
    user = getpass.getuser().upper()
    dst = Path("notebooks") / f"{now}_{user}_{desc_underscore}.ipynb"
    print(src, " -->", dst)
    with open(src, "r") as f:
        rendered = Template(f.read()).render(title=description, date=now)
    with open(dst, "w") as f:
        f.write(rendered)


if __name__ == "__main__":
    main()
