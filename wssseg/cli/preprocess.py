# -*- coding: utf-8 -*-
import logging
from pathlib import Path
from typing import List

import click
import click_log
import numpy as np
import torch
from pelper import pipe
from PIL import Image
from torchvision.transforms import functional as TF

from wssseg import datasets
from wssseg.utils import get_project_root, tqdm

logger = logging.getLogger(__name__)
click_log.basic_config(logger)


########################################################################################
# MASKS
def create_masks(mask_paths: List[Path], dst_dir: Path):
    dst_dir.mkdir(parents=True, exist_ok=True)
    for src in mask_paths:
        dst = dst_dir / src.name
        print(src, "-->", dst)

        pipe(
            str(src),
            Image.open,
            TF.to_tensor,
            maskify,
            TF.to_pil_image,
            lambda img: img.save(dst),
        )


def maskify(img_mask: torch.Tensor) -> torch.Tensor:
    OBJECTS = [
        ("impervious", (1, 1, 1), 0),  # WHITE
        ("building", (0, 0, 1), 1),  # BLUE
        ("low_vegetation", (0, 1, 1), 2),  # TURQUOISE
        ("tree", (0, 1, 0), 3),  # GREEN
        ("car", (1, 1, 0), 4),  # YELLOW
        ("clutter", (1, 0, 0), 255),  # RED
        # ("clutter", (1, 0, 0), 5),       # RED
    ]

    np_mask = img_mask.numpy()
    result_mask = np.zeros(np_mask.shape[1:], dtype=np.uint8)
    for (obj_name, color, id_) in OBJECTS:
        color = np.array(color)[:, None, None]
        obj_mask = np.all(np_mask == color, axis=0)
        result_mask[obj_mask] = id_
        print(id_, obj_mask.sum())
    return torch.from_numpy(result_mask)


########################################################################################
# TILES
def create_tiles(images: List[Path], dst_dir: Path) -> None:
    dst_dir.mkdir(parents=True)
    for img in tqdm(images):
        _create_tiles(img, dst_dir)


def _create_tiles(img_path: Path, dst_dir, size=224):
    logger.debug(f"Processing {img_path}")
    image = Image.open(img_path)
    if image.size[0] % size != 0 or image.size[1] % size != 0:
        logger.debug(
            f"Loosing parts of the image '{img_path}':\n"
            f"  {image.size[0] % size} x pixels\n"
            f"  {image.size[1] % size} y pixels"
        )

    x, y = 0, 0
    i = 0
    while (y + size) < image.size[1]:
        while (x + size) < image.size[0]:
            tile = image.crop((x, y, x + size, y + size))
            dst = dst_dir / (img_path.with_suffix("").name + f"_x{x}_y{y}.tif")
            logger.debug(f"{i} {x} {y} {dst}")
            tile.save(str(dst))
            x += size
            i += 1
        y += size
        x = 0


########################################################################################
# CLI
@click.command()
@click_log.simple_verbosity_option(logger)
def main():
    """CLI demo."""
    logger = logging.getLogger(__name__)
    logger.debug("# Preprocessing...")

    # TODO The paths here should not be hardcoded! ...if there is more time.
    root = get_project_root()
    # images = (root / "data/raw/top/").ls
    mask_files = (root / "data/raw/ground_truth/").ls

    # MASKS
    dst_dir = root / "data/raw/masks"
    if not dst_dir.exists():
        create_masks(mask_files, dst_dir=root / "data/raw/masks")
    else:
        print(f"# Skipping create_masks for {dst_dir}")

    # TILES
    for _dst, src, dataset in [
        ("data/interim/n1_images", "data/raw/top", datasets.n1),
        ("data/interim/n1_masks", "data/raw/masks", datasets.n1),
        ("data/interim/n2_images", "data/raw/top", datasets.n2),
        ("data/interim/n2_masks", "data/raw/masks", datasets.n2),
        ("data/interim/nval_images", "data/raw/top", datasets.nval),
        ("data/interim/nval_masks", "data/raw/masks", datasets.nval),
    ]:
        dst_dir = root / _dst
        if not dst_dir.exists():
            create_tiles([(root / src / p) for p in dataset], dst_dir)
        else:
            print(f"# Skipping create_tiles for {dst_dir}")


if __name__ == "__main__":
    main()
