import datetime
import time
from pathlib import Path

import neptune
import torch
import torch.utils.data
import torchvision
from matplotlib import pyplot as plt
from torch import nn

import wssseg.torchseg.transforms as T
from wssseg import datasets
from wssseg.experiment import track_exp
from wssseg.torch_utils import denormalize, freeze_all, get_trainable, imagenet_stats
from wssseg.torchseg import utils
from wssseg.utils import get_project_root


def get_dataset(ds_name: str, transforms):
    _sets = {
        "n1": "data/interim/n1",
        "n2": "data/interim/n2",
        "nval": "data/interim/nval",
    }
    assert ds_name in _sets, ds_name

    root = get_project_root()
    top_dir = root / (_sets[ds_name] + "_images")
    mask_dir = root / (_sets[ds_name] + "_masks")
    images = top_dir.ls
    masks = mask_dir.ls

    assert all([p.is_file() for p in images])
    assert all([p.is_file() for p in masks])

    ds = datasets.VaihingenSegmentationDataset(images, masks, transforms)
    assert len(ds) > 0

    num_classes = 6
    return ds, num_classes


def get_transform(train: bool) -> T.Compose:
    transforms = []
    if train:
        transforms.append(T.RandomHorizontalFlip(0.5))
        transforms.append(T.RandomVerticalFlip(0.5))
        # TODO more augmentation
    transforms.append(T.ToTensor())
    transforms.append(T.Normalize(*imagenet_stats))
    return T.Compose(transforms)


def get_model(model_name: str, num_classes: int, pretrained: bool = False) -> nn.Module:
    if not pretrained:
        model = torchvision.models.segmentation.__dict__[model_name](
            num_classes=num_classes, aux_loss=False, pretrained=False
        )
    else:
        model = torchvision.models.segmentation.__dict__[model_name](
            num_classes=21, aux_loss=False, pretrained=True
        )
        model.classifier = torchvision.models.segmentation.fcn.FCNHead(
            2048, num_classes
        )
    return model


def criterion(inputs, target):
    losses = {}
    for name, x in inputs.items():
        losses[name] = nn.functional.cross_entropy(x, target, ignore_index=255)

    if len(losses) == 1:
        return losses["out"]

    return losses["out"] + 0.5 * losses["aux"]


def evaluate(model, data_loader, device, num_classes: int, epoch: int):
    model.eval()
    confmat = utils.ConfusionMatrix(num_classes)
    metric_logger = utils.MetricLogger(delimiter="  ")
    header = "Test:"
    with torch.no_grad():
        for image, target in metric_logger.log_every(data_loader, 100, header):
            image, target = image.to(device), target.to(device)
            output = model(image)
            output = output["out"]

            confmat.update(target.flatten(), output.argmax(1).flatten())

        confmat.reduce_from_all_processes()

    _log_metric_logger(metric_logger, epoch, prefix="valid")
    _log_confmat(confmat, epoch)
    return confmat


def visualize(model, data_loader, epoch: int, device, output_dir: Path) -> None:
    model.eval()
    prediction_dir = output_dir / f"prediction_{epoch}"
    prediction_dir.mkdir(parents=True, exist_ok=True)
    for (image, target), path in zip(data_loader, data_loader.dataset.masks):
        image, target = image.to(device), target.to(device)
        out = model(image)["out"].cpu()
        mask = torch.argmax(out, dim=1)

        fig, axes = plt.subplots(1, 3, figsize=(12, 4))
        fig.suptitle(str(path.name))

        image = denormalize(image.cpu().squeeze(0), imagenet_stats)
        axes[0].imshow(image.permute(1, 2, 0))
        axes[0].axis("off")
        axes[0].set_title("image")

        axes[1].imshow(target.cpu().squeeze(0))
        axes[1].axis("off")
        axes[1].set_title("ground truth")

        axes[2].imshow(mask.cpu().squeeze(0))
        axes[2].axis("off")
        axes[2].set_title("pred")

        dst = prediction_dir / path.name
        fig.savefig(str(dst))
        plt.close(fig)


def train_one_epoch(
    model, criterion, optimizer, data_loader, lr_scheduler, device, epoch, print_freq
):
    model.train()
    metric_logger = utils.MetricLogger(delimiter="  ")
    metric_logger.add_meter("lr", utils.SmoothedValue(window_size=1, fmt="{value}"))
    header = "Epoch: [{}]".format(epoch)
    for image, target in metric_logger.log_every(data_loader, print_freq, header):
        image, target = image.to(device), target.to(device)
        output = model(image)
        loss = criterion(output, target)

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        lr_scheduler.step()

        metric_logger.update(loss=loss.item(), lr=optimizer.param_groups[0]["lr"])

    _log_metric_logger(metric_logger, epoch, prefix="train")


def _log_metric_logger(metric_logger: utils.MetricLogger, epoch: int, prefix: str):
    for meter_name, meter in metric_logger.meters.items():
        neptune.log_metric(f"{prefix}_{meter_name}_value", epoch, meter.value)
        neptune.log_metric(f"{prefix}_{meter_name}_avg", epoch, meter.avg)


def _log_confmat(convmat: utils.ConfusionMatrix, epoch: int):
    acc_global, acc, iu = convmat.compute()
    neptune.log_metric("global_correct", epoch, acc_global.item() * 100)
    neptune.log_metric("mean IoU", epoch, iu.mean().item() * 100)
    for i, _acc in enumerate(acc):
        neptune.log_metric(f"avg_row_correct_{i}", epoch, _acc.cpu().item() * 100)
    for i, _iu in enumerate(iu):
        neptune.log_metric(f"IoU_{i}", epoch, _iu.cpu().item() * 100)


def main(args):
    device = torch.device(args.device)

    dataset, num_classes = get_dataset(args.ds_name, get_transform(train=True))
    dataset_test, _ = get_dataset("nval", get_transform(train=False))

    train_sampler = torch.utils.data.RandomSampler(dataset)
    test_sampler = torch.utils.data.SequentialSampler(dataset_test)

    data_loader = torch.utils.data.DataLoader(
        dataset,
        batch_size=args.batch_size,
        sampler=train_sampler,
        num_workers=args.workers,
        collate_fn=utils.collate_fn,
        drop_last=False,
    )

    data_loader_test = torch.utils.data.DataLoader(
        dataset_test,
        batch_size=1,
        sampler=test_sampler,
        num_workers=args.workers,
        collate_fn=utils.collate_fn,
    )

    model = get_model(args.model, num_classes, args.pretrained)
    model.to(device)

    # resume from pretrained backbone
    if args.resume:
        checkpoint = torch.load(args.resume, map_location="cpu")
        backbone_state_dict = {
            name: weight
            for name, weight in checkpoint["model"].items()
            if name.startswith("backbone")
        }
        model.load_state_dict(backbone_state_dict, strict=False)

    if args.freeze_backbone:
        freeze_all(model.backbone.parameters())

    model_without_ddp = model

    if args.test_only:
        confmat = evaluate(
            model, data_loader_test, device=device, num_classes=num_classes, epoch=None
        )
        print(confmat)
        return

    params_to_optimize = [
        {"params": list(get_trainable(model_without_ddp.backbone.parameters()))},
        {"params": list(get_trainable(model_without_ddp.classifier.parameters()))},
    ]
    if args.aux_loss:
        params = list(get_trainable(model_without_ddp.aux_classifier.parameters()))
        params_to_optimize.append({"params": params, "lr": args.lr * 10})
    optimizer = torch.optim.SGD(
        params_to_optimize,
        lr=args.lr,
        momentum=args.momentum,
        weight_decay=args.weight_decay,
    )

    lr_scheduler = torch.optim.lr_scheduler.LambdaLR(
        optimizer, lambda x: (1 - x / (len(data_loader) * args.epochs)) ** 0.9
    )

    tags = ["WIP", args.ds_name, "weakly_pretrained"]
    with track_exp(tags=tags, params=vars(args)) as output_dir:
        start_time = time.time()
        for epoch in range(args.epochs):
            train_one_epoch(
                model,
                criterion,
                optimizer,
                data_loader,
                lr_scheduler,
                device=device,
                epoch=epoch,
                print_freq=args.print_freq,
            )
            confmat = evaluate(
                model=model,
                data_loader=data_loader_test,
                device=device,
                num_classes=num_classes,
                epoch=epoch,
            )
            print(confmat)
            utils.save_on_master(
                {
                    "model": model_without_ddp.state_dict(),
                    "optimizer": optimizer.state_dict(),
                    "epoch": epoch,
                    "args": args,
                },
                str(output_dir / f"model_{epoch}.pth"),
            )

        visualize(model, data_loader_test, epoch, device, output_dir=output_dir)

        total_time = time.time() - start_time
        total_time_str = str(datetime.timedelta(seconds=int(total_time)))
        print("Training time {}".format(total_time_str))


def parse_args():
    import argparse

    parser = argparse.ArgumentParser(description="PyTorch Segmentation Training")

    # parser.add_argument("--dataset", default="voc", help="dataset")
    parser.add_argument("--model", default="fcn_resnet101", help="model")
    parser.add_argument("--aux-loss", action="store_true", help="auxiliar loss")
    parser.add_argument("--device", default="cuda", help="device")
    parser.add_argument("-b", "--batch-size", default=8, type=int)
    parser.add_argument(
        "--epochs",
        default=30,
        type=int,
        metavar="N",
        help="number of total epochs to run",
    )

    parser.add_argument(
        "-j",
        "--workers",
        default=16,
        type=int,
        metavar="N",
        help="number of data loading workers (default: 16)",
    )
    parser.add_argument("--lr", default=0.01, type=float, help="initial learning rate")
    parser.add_argument(
        "--momentum", default=0.9, type=float, metavar="M", help="momentum"
    )
    parser.add_argument(
        "--wd",
        "--weight-decay",
        default=1e-4,
        type=float,
        metavar="W",
        help="weight decay (default: 1e-4)",
        dest="weight_decay",
    )
    parser.add_argument("--print-freq", default=10, type=int, help="print frequency")
    parser.add_argument("--resume", default="", help="resume from checkpoint")
    parser.add_argument(
        "--test-only", dest="test_only", help="Only test the model", action="store_true"
    )
    parser.add_argument(
        "--pretrained",
        dest="pretrained",
        help="Use pre-trained models from the modelzoo",
        action="store_true",
    )
    parser.add_argument(
        "--freeze-backbone",
        dest="freeze_backbone",
        help="Freeze the backbone.",
        action="store_true",
    )
    parser.add_argument(
        "--ds-name",
        dest="ds_name",
        help="The name of the dataset: n1, n2.",
        type=str,
        default="n1",
    )

    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = parse_args()
    main(args)
