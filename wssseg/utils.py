import datetime
import hashlib
import importlib
import re
from contextlib import ContextDecorator
from functools import wraps
from itertools import tee
from pathlib import Path
from typing import Any, Callable, Dict, Generator, Iterable, List, Optional

import numpy as np
import tomlkit

###############################################################################
# Automatically import the correct tqdm.
# Use it like this:
#
#     from ml_playground.utils import tqdm
#
from IPython import get_ipython
from IPython.display import HTML, display
from toolz.dicttoolz import merge, merge_with

_ipython = get_ipython()
if _ipython is None:
    from tqdm import tqdm  # noqa
elif _ipython.__class__.__name__ == "ZMQInteractiveShell":
    from tqdm import tqdm_notebook as tqdm  # noqa
else:
    from tqdm import tqdm  # noqa
del _ipython


###############################################################################
Path.ls = property(lambda path: sorted(list(path.iterdir())))  # noqa


###############################################################################
# Jupyter helper
def h1(text):
    display(HTML(f"<h1>{text}</h1>"))


def h2(text):
    display(HTML(f"<h2>{text}</h2>"))


###############################################################################
# File verification
def calc_sha(path: Path):
    path = Path(path)
    sha = hashlib.sha1()
    with path.open("rb") as f:
        sha.update(f.read())
    digest = sha.hexdigest()
    return digest


def verify_sha(path: Path, sha1_checksum: str):
    digest = calc_sha(path)
    if digest != sha1_checksum:
        print(f"ERROR: {path} has sha '{digest}' but '{sha1_checksum}' expected")
        return False
    return True


###############################################################################
# numpy helpers
def store_np(np_arr: np.ndarray, dst: str):
    """Store the numpy array, create missing folders."""
    dst = Path(dst)
    if dst.is_file():
        print(f"Aborting, '{dst}' already exists.")
        return

    print(f"Creating '{dst}'...")
    dst.parent.mkdir(exist_ok=True, parents=True)
    with dst.open("wb") as f:
        np.save(f, np_arr)


########################################################################################
# Decorators
def lazy_property(fn: Callable) -> Callable:
    """Decorator that makes a property lazy-evaluated."""
    attr_name = "_lazy_" + fn.__name__

    @property
    def _lazy_property(self):
        if not hasattr(self, attr_name):
            setattr(self, attr_name, fn(self))
        return getattr(self, attr_name)

    return _lazy_property


def return_fnname(f):
    """The decorated function returns a tuple of the called function and the name of
    the function.

    Examples:
        >>> @return_fnname
        ... def f(): return "hello"
        >>> result, name = f()
        >>> name
        'f'
        >>> result
        'hello'
    """

    @wraps(f)
    def wrap():
        return f(), f.__name__

    return wrap


########################################################################################
# Misc
def get_project_root() -> Path:
    """Return the root of the current project."""
    return Path(__file__).parent.parent


def import_from_module(something: str, module_str: str) -> Any:
    """Import `something` from the given `module_str` and return it."""
    return getattr(importlib.import_module(module_str), something)


def class_name(cls_or_instance: Any) -> str:
    """Return the class name of the given class or instance.

    Examples:
        >>> class Foo: pass
        >>> class_name(Foo)
        'Foo'
        >>> class_name(Foo())
        'Foo'
    """
    return (
        cls_or_instance.__name__
        if hasattr(cls_or_instance, "__name__")
        else cls_or_instance.__class__.__name__
    )


def snake_case(x: str) -> str:
    """
    Examples:
        >>> snake_case('CamelCase')
        'camel_case'
        >>> snake_case("snakesOnAPlane")
        'snakes_on_a_plane'
        >>> snake_case("SnakesOnAPlane")
        'snakes_on_a_plane'
        >>> snake_case("snakes_on_a_plane")
        'snakes_on_a_plane'
        >>> snake_case("IPhoneHysteria")
        'i_phone_hysteria'
        >>> snake_case("iPhoneHysteria")
        'i_phone_hysteria'
        >>> snake_case('test title')
        'test_title'
        >>> snake_case('Test Title')
        'test_title'
    """
    x = re.sub("(.)([A-Z][a-z]+)", r"\1_\2", x)
    x = re.sub("([a-z0-9])([A-Z])", r"\1_\2", x).lower()
    x = re.sub(" ", "_", x)
    x = re.sub("_+", "_", x)
    return x


def now_str() -> str:
    """Create a str representing the current time (now) with this format:
    '2017-01-11_14:41:33'
    """
    return datetime.datetime.now().isoformat("_", "seconds")


class time_it(ContextDecorator):
    """
    Measure the execution time of a decorated function or a context.

    `time_it` can be used as context manager or as decorator.

    Args:
        do_print: Print the duration if `True` (default).

    Examples:

        >>> @time_it()
        ... def f():
        ...     print("foo")
        >>> f()
        foo
        Took ...
        >>> with time_it():
        ...     print("foo")
        foo
        Took ...
        >>> with time_it(do_print=False) as t:
        ...     print("foo")
        foo
        >>> isinstance(t.duration, datetime.timedelta)
        True
    """

    def __init__(self, do_print: bool = True):
        self.do_print = do_print
        self.start_ts = None
        self.end_ts = None
        self.duration = None

    def __enter__(self) -> "time_it":
        self.start_ts = datetime.datetime.now()  # type: ignore
        return self

    def __exit__(self, *exc):
        self.end_ts = datetime.datetime.now()
        self.duration = self.end_ts - self.start_ts
        if self.do_print:
            print("Took", self.duration)
        return False


def pairwise(iterable: Iterable):
    """Iterate over pairs of `iterable`.

    s -> (s0,s1), (s1,s2), (s2, s3), ..."

    Examples:
        >>> for a,b in pairwise([1, 2, 3]):
        ...     print(a, b)
        1 2
        2 3
    """
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)


def first_not_none(*args: Any):
    """Return the first agument that is not None.

    Examples:
        >>> first_not_none(None, None, "a", None)
        'a'
        >>> first_not_none(None, "b", "a", None)
        'b'
        >>> first_not_none({}, "b", "a", None)
        {}
    """
    for arg in args:
        if arg is not None:
            return arg


def flat_items(d: Dict, sep: str = "/", prefix: str = None) -> Generator:
    """Like `dict.items()` but flatten all values recursively.

    Args:
        d: a potentially nested dict.
        sep: the separator used to build the key `f"{prefix}{sep}{key}` when flattening.
        prefix: the prefix used to build the key `f"{prefix}{sep}{key}` when flattening.

    Returns:
        Generator of the flattened dict items.

    Examples:
        >>> flat_items({})
        <generator object flat_items at ...>
        >>> list(flat_items({}))
        []
        >>> list(flat_items({1: 2, 2: 3}))
        [(1, 2), (2, 3)]
        >>> list(flat_items({1: 2, 2: 3, "train": {"acc": 0.9}}))
        [(1, 2), (2, 3), ('train/acc', 0.9)]
        >>> list(flat_items({1: 2, 2: 3, "train": {"acc": 0.9, "loss": 0.12}}))
        [(1, 2), (2, 3), ('train/acc', 0.9), ('train/loss', 0.12)]
        >>> g = flat_items({1: 2, "train": {"acc": 0.9, "loss": {"kl": 0.1, "l2": 0.3}}})
        >>> list(g)
        [(1, 2), ('train/acc', 0.9), ('train/loss/kl', 0.1), ('train/loss/l2', 0.3)]
    """
    for k, v in d.items():
        k = k if prefix is None else f"{prefix}{sep}{k}"
        if isinstance(v, dict):
            yield from flat_items(v, sep=sep, prefix=k)
        else:
            yield (k, v)


def listify(obj: Any) -> List[Any]:
    """Turn obj into a list.

    Examples:
        >>> listify(None)
        []
        >>> listify("test")
        ['test']
        >>> listify(1)
        [1]
        >>> listify([])
        []
        >>> listify([1])
        [1]
        >>> listify([1, 2])
        [1, 2]
        >>> listify(range(2))
        [0, 1]

    """
    if obj is None:
        return []
    elif isinstance(obj, list):
        return obj
    elif isinstance(obj, str):
        return [obj]
    elif isinstance(obj, Iterable):
        return list(obj)
    else:
        return [obj]


def deep_merge(*dicts: Dict) -> Dict:
    """Deep merge multiple dicts and return the merged dict.

    Examples:
        >>> # works with not nested dicts
        >>> deep_merge({1: 1}, {2: 2}, {3: 3})
        {1: 1, 2: 2, 3: 3}
        >>> # and works with nested dicts
        >>> deep_merge({1: {1: 1}}, {2: {2: 2}})
        {1: {1: 1}, 2: {2: 2}}
        >>> deep_merge({1: {1: 1}}, {1: {2: 2}})
        {1: {1: 1, 2: 2}}
        >>> deep_merge({1: {1: 1}}, {1: {2: 2}, 2: {"a": "a"}})
        {1: {1: 1, 2: 2}, 2: {'a': 'a'}}
        >>> deep_merge({"a": {1: {1: 1}, 2:{2: 2}}, "b": {1: {1: 1}}}, {"c": {1: {1: 1}}})
        {'a': {1: {1: 1}, 2: {2: 2}}, 'b': {1: {1: 1}}, 'c': {1: {1: 1}}}
        >>> deep_merge({"a": {1: {1: 1}, 2:{2: 2}}, "b": {1: {1: 1}}}, {"c": {1: {1: 1}}, "b":{1: {1: "x"}, "x": {"x": "x"}}})
        {'a': {1: {1: 1}, 2: {2: 2}}, 'b': {1: {1: 'x'}, 'x': {'x': 'x'}}, 'c': {1: {1: 1}}}
        >>> deep_merge({"a": {1: {1: 1}, 2: {}, 3: {} }}, {"a": {2: {1: {}}}})
        {'a': {1: {1: 1}, 2: {1: {}}, 3: {}}}
        >>> deep_merge({"a": {1: {1: 1}, 2: {}, 3: {} },"b": {"x": "x"}}, {"a": {2: {1: {}}}})
        {'a': {1: {1: 1}, 2: {1: {}}, 3: {}}, 'b': {'x': 'x'}}
    """
    try:
        return merge_with(merge, *dicts)
    except TypeError:
        return merge(*dicts)


def attr(obj: Any) -> List[str]:
    """`attr` is like `dir`, but filters the dounder attributes."""
    return [x for x in dir(obj) if not x.startswith("__")]


def assert_keys(
    dict_, required_keys: Iterable[str], hint: Optional[str] = None
) -> bool:
    """Check that all `required_keys` are in `dict_`.

    Args:
        dict_: the dict to check.
        required_keys: the keys that are supposed to be in dict_.
        hint: An optional hint that is shown in the error case.

    Examples:
        >>> assert_keys({"a": "a"}, ["a"])
        True
        >>> assert_keys({"a": 1, "b": 2}, ["a"])
        True
        >>> assert_keys({"a": 1, "b": 2}, ["a", "b"])
        True
        >>> assert_keys({"a": 1, "b": 2}, ["a", "b", "c"])
        Traceback (most recent call last):
            ...
        AssertionError: The following keys are missing: ['c'].
        >>> assert_keys({"a": 1, "b": 2}, ["a", "b", "c"], hint="Fix it like this.")
        Traceback (most recent call last):
            ...
        AssertionError: The following keys are missing: ['c'].
          Fix it like this.
    """
    keys = dict_.keys()
    missing_keys = [k for k in required_keys if k not in keys]
    if missing_keys:
        msg = f"The following keys are missing: {missing_keys}."
        if hint:
            msg += f"\n  {hint}"
        raise AssertionError(msg)
    return True


########################################################################################
# CONFIG MANAGEMENT
class BaseConfig:
    """
    A simple config implementation.

    EXAMPLES:
        Use it like this:

        >>> from dataclasses import dataclass
        >>> @dataclass
        ... class Config(BaseConfig):
        ...     LR: float = 0.01
        ...     EPOCHS: int = 5
    """

    def update_from_dict(self, dict_):
        """Update the config with the given dict_."""
        for key in dict_:
            if hasattr(self, key):
                setattr(self, key, dict_[key])
            else:
                print(f"Adding new key '{key}'...")
                setattr(self, key, dict_[key])
        return self

    def update_from_toml(self, toml_file="config.toml", section=None):
        """Update the config with the given toml_file (and section if specified)."""
        with open(toml_file) as f:
            toml_config = tomlkit.parse(f.read())
        if section is not None:
            toml_config = toml_config[section]
        dict_ = {
            key: value
            for key, value in toml_config.items()
            if isinstance(value, (str, int, float))
        }
        return self.update_from_dict(dict_)
