import matplotlib.pyplot as plt


def plot_image_array(image_array):
    """
    Plot a nested array/list of lists of images.

    Images must be ndarrays.

    You can add a title for each image by passing a tuple of (img, str)
    instead of the pure image.
    You can also just print the title, but no image by setting the image
    of the tule to `None`.

    ::

        [
            [(img1, "Title 1"), (img2, "Title 2")],
            [img3],  # no title
            [(None, "Title but no image"],
        ],

    """
    n_rows = len(image_array)
    n_cols = max(len(row) for row in image_array)

    fig, axes = plt.subplots(n_rows, n_cols, figsize=(n_cols * 2, n_rows * 2))
    [ax.axis("off") for ax in fig.axes]

    for i, image_row in enumerate(image_array):
        for j, img in enumerate(image_row):
            if isinstance(img, tuple):
                img, title = img
                axes[i, j].set_title(title)
            if img is None:
                continue
            axes[i, j].imshow(img)

    return fig, axes
