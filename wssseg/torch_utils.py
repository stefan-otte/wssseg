import multiprocessing
import zipfile
from collections import OrderedDict, namedtuple
from pathlib import Path
from typing import Callable, Dict, Iterator, List, Tuple

import numpy as np
import torch
import torch.nn as nn
from dataclasses import dataclass, field
from PIL import Image
from torch import optim
from torch.utils.data import DataLoader, Dataset
from torchvision import transforms
from torchvision.datasets.folder import ImageFolder, default_loader
from torchvision.datasets.utils import check_integrity, download_url

########################################################################################
# Types
ParameterIter = Iterator[nn.Parameter]

ImageStats = namedtuple("ImageStats", "mean std")
imagenet_stats = ImageStats(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])


########################################################################################
# Misc
DEVICE = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
CPU_COUNT = multiprocessing.cpu_count()


def normalizator_factory(*, mean=None, std=None):
    """
    Generate a normalizer and the corresponding de-normalizer.

    normalizer nad de-normalizer are pytorch `transform` objects.
    """
    # the standard pytorch mean and std
    if mean is None:
        mean = (0.485, 0.456, 0.406)
    if std is None:
        std = (0.229, 0.224, 0.225)

    mean = np.asarray(mean)
    std = np.asarray(std)

    normalizer = transforms.Normalize(mean, std)
    denormalizer = transforms.Compose(
        [
            transforms.Normalize(mean=(0.0, 0.0, 0.0), std=np.array(1) / std),
            transforms.Normalize(mean=-mean, std=(1, 1, 1)),
        ]
    )
    return normalizer, denormalizer


def denormalize(
    tensor: torch.Tensor, stats: ImageStats = imagenet_stats
) -> torch.Tensor:
    """Denormalize the given tensor with the given stats.

    Examples:
        >>> x = torch.rand(3, 224, 224)
        >>> denormalize(x, imagenet_stats).shape
        torch.Size([3, 224, 224])
    """
    std = torch.tensor(stats.std)[:, None, None]
    mean = torch.tensor(stats.mean)[:, None, None]
    return (tensor * std) + mean


def get_mean_and_std(dataloader: DataLoader) -> Tuple[torch.Tensor, torch.Tensor]:
    """
    Return the mean and the std for the given dataloader.

    Note: you can also subsample the dataloader to get an approximation of the stats.
    """
    assert isinstance(dataloader, DataLoader)
    means, stds = [], []
    for X, _ in dataloader:
        assert X.dim() == 4
        X = X.flatten(start_dim=2, end_dim=-1)
        means.append(X.mean(-1))
        stds.append(X.std(-1))
    mean = torch.cat(means).mean(0)
    std = torch.cat(stds).mean(0)
    return mean, std


def tensorinfo_hook(self, input_, output):
    """
    Register this forward hook to print some infos about the tensor/module.

    Example:

        >>> from torchvision.models import resnet18
        >>> model = resnet18(pretrained=False)
        >>> hook_fc = model.fc.register_forward_hook(tensorinfo_hook)
        >>> hook_l4 = model.layer4.register_forward_hook(tensorinfo_hook)
        >>> # model(torch.ones(1, 3, 244, 244))

    """
    print(f"Inside '{self.__class__.__name__}' forward")
    print(f"  input:     {str(type(input_)):<25}")
    print(f"  input[0]:  {str(type(input_[0])):<25} {input_[0].size()}")
    print(f"  output:    {str(type(output)):<25} {output.data.size()}")
    print()


def to_multi_hot(x: torch.Tensor, n_classes: int) -> torch.Tensor:
    """Multi hot  encode the given tensor `x`.

    Like one hot but for multi label classification.

    Examples:
        >>> x = torch.tensor([[]]).long()
        >>> to_multi_hot(x, n_classes=3)
        tensor([[0., 0., 0.]])

        >>> x = torch.tensor([[0, 1, 2]])
        >>> to_multi_hot(x, n_classes=3)
        tensor([[1., 1., 1.]])

        >>> x = torch.tensor([[2]])
        >>> to_multi_hot(x, n_classes=3)
        tensor([[0., 0., 1.]])

        >>> x = torch.tensor([[2, 0]])
        >>> to_multi_hot(x, n_classes=3)
        tensor([[1., 0., 1.]])


        >>> x = torch.tensor([[2, 0, 255]])
        >>> to_multi_hot(x, n_classes=3)
        tensor([[1., 0., 1.]])
    """
    assert x.dim() == 2, f"Should be 2, is {x.dim()} - {x.shape}"
    # TODO find a better implementation
    # return torch.zeros(x.size(0), n_classes)[[val for val in x[0]]] = 1.0
    # .scatter(1, x, 1.0)
    result = torch.zeros(x.size(0), n_classes)
    result[0, [val for val in x[0] if val < n_classes]] = 1.0
    return result


def cos_sim(a: torch.Tensor, b: torch.Tensor, eps: float = 1e-8) -> torch.Tensor:
    """Calculate the pairwise cosine similarity from `a` to `b`, i.e.,
    calculate the cosine similarity for each row in `a` to each row in `b`
    returning a tensor of shape `(a.shape[0], b.shape[1])`.

    Note: this is a generalized version of `torch.nn.functional.cosine_similarity`.
    """
    if a.dim() != 2 or b.dim() != 2:
        raise ValueError(f"`a` and `b` must be of dim 2. Is: a={a.dim()} b={b.dim()}")
    if a.shape[1] != a.shape[1]:
        raise ValueError(
            "`a` and `b` must have the same second dimention."
            f" Is: a={a.shape} b={b.shape}."
        )

    a_norm = a.norm(p=2, dim=1, keepdim=True)
    b_norm = b.norm(p=2, dim=1, keepdim=True)
    magnitude = (a_norm * b_norm.t()).clamp(min=eps)
    return (a @ b.t()) / magnitude


def cos_dist(a: torch.Tensor, b: torch.Tensor, eps: float = 1e-8) -> torch.Tensor:
    """`1 - cos_sim(a, b)`"""
    return 1 - cos_sim(a, b, eps)


########################################################################################
# Layer freezing
def get_trainable(model_params: ParameterIter) -> ParameterIter:
    """Return a generator of the trainable parameters."""
    assert not isinstance(model_params, nn.Module)
    return (p for p in model_params if p.requires_grad)


def get_frozen(model_params: ParameterIter) -> ParameterIter:
    """Return a generator of the frozen parameters."""
    assert not isinstance(model_params, nn.Module)
    return (p for p in model_params if not p.requires_grad)


def all_trainable(model_params: ParameterIter) -> bool:
    """Return `True` if all parameters are trainable, `False` otherwise."""
    assert not isinstance(model_params, nn.Module)
    return all(p.requires_grad for p in model_params)


def all_frozen(model_params: ParameterIter) -> bool:
    """Return `True` if all parameters are frozen, `True` otherwise."""
    assert not isinstance(model_params, nn.Module)
    return all(not p.requires_grad for p in model_params)


def freeze_all(model_params: ParameterIter) -> None:
    """Freeze the given parameters."""
    assert not isinstance(model_params, nn.Module)
    for param in model_params:
        param.requires_grad = False


def unfreeze_all(model_params: ParameterIter) -> None:
    """Unfreeze the given parameters."""
    assert not isinstance(model_params, nn.Module)
    for param in model_params:
        param.requires_grad = True


def freeze(module: nn.Module, to=-1) -> None:
    """Freeze the the children of `module` to `to`.

    Examples:
        >>> module = nn.Sequential(nn.Linear(1, 2), nn.Linear(2, 1))
        >>> all_trainable(module.parameters())
        True
        >>> freeze(module)
        Freezing till layer -1...
        >>> all_trainable(module.parameters())
        False
        >>> freeze_info(module)
        layer frozen?      shape
        ---------------------------------
            0   True  torch.Size([2, 1])
            1   True  torch.Size([2])
            2   False  torch.Size([1, 2])
            3   False  torch.Size([1])

        >>> freeze(module, -2)
        Freezing till layer -2...
        >>> freeze_info(module)
        layer frozen?      shape
        ---------------------------------
            0   False  torch.Size([2, 1])
            1   False  torch.Size([2])
            2   False  torch.Size([1, 2])
            3   False  torch.Size([1])
    """
    assert isinstance(module, nn.Module)
    print(f"Freezing till layer {to}...")
    children = list(module.children())
    for l in children[:to]:
        freeze_all(l.parameters())
    for l in children[to:]:
        unfreeze_all(l.parameters())


def unfreeze(module: nn.Module) -> None:
    """Unfreeze the entire given `module`."""
    assert isinstance(module, nn.Module)
    unfreeze_all(module.parameters())


def freeze_info(module: nn.Module) -> None:
    """Which parameters of the module are trainable? Print infos about it.

    >>> model = nn.Sequential(nn.Linear(2, 3))
    >>> freeze_info(model)
    layer frozen?      shape
    ---------------------------------
        0   False  torch.Size([3, 2])
        1   False  torch.Size([3])

    >>> freeze_all(model.parameters())
    >>> freeze_info(model)
    layer frozen?      shape
    ---------------------------------
        0   True  torch.Size([3, 2])
        1   True  torch.Size([3])

    """
    assert isinstance(module, nn.Module)
    print("layer frozen?      shape")
    print("---------------------------------")
    for i, param in enumerate(module.parameters()):
        print(f"{i:>5d}   {not param.requires_grad}  {param.shape}")


def take_layers_till(module: nn.Module, layer_name: str) -> Dict[str, nn.Module]:
    """
    Extract and return all layers *including* the layer with the name `layer_name` from
    the given `module` as OrderedDict.

    Examples:
        >>> from torchvision.models import resnet18
        >>> nn.Sequential(take_layers_till(resnet18(), "conv1"))
        Sequential(
          (conv1): Conv2d(3, 64, kernel_size=(7, 7), stride=(2, 2), padding=(3, 3), bias=False)
        )
    """
    layers = []
    for cur_name, cur_layer in module.named_children():
        layers.append((cur_name, cur_layer))
        if cur_name == layer_name:
            break
    return OrderedDict(layers)


def set_lr(optimizer: optim.Optimizer, lr: float) -> None:
    """Set the learning rate to `lr` for all param groups of the given `optimizer`."""
    for group in optimizer.param_groups:
        assert "lr" in group, f"'lr' not part of the param_group: {group}"
        group["lr"] = lr


########################################################################################
# Datasets
class ImageListDataset(Dataset):
    """A simple image dataset (without labels).

    Note the `image(idx)` method which always returns the (untransformed) PIL.Image
    representation.
    """

    def __init__(self, image_paths, transform: Callable = None) -> None:
        self.image_paths = image_paths
        self.transform = transform

    def image(self, idx) -> Image:
        return Image.open(self.image_paths[idx])

    def __len__(self) -> int:
        return len(self.image_paths)

    def __getitem__(self, idx: int):
        X = self.image(idx)
        if self.transform:
            X = self.transform(X)
        return X


class DogsCatsDataset(ImageFolder):
    """
    The 'Dogs and Cats' dataset from kaggle.

    https://www.kaggle.com/c/dogs-vs-cats-redux-kernels-edition/


    Args:
        train_val: path to the train/valid/sample dataset. See folder structure.
        transform (Callable, optional): A function/transform that takes in
            an PIL image and returns a transformed version.
            E.g, ``transforms.RandomCrop``.
        target_transform (Callable, optional): A function/transform that
            takes in the target and transforms it.
        loader: A function to load an image given its path.
        download: if ``True``, download the data.
        root: the location of the dataset `<root>/dogscats/`, `~/data/` by default.


    The folder structure of the dataset is as follows::

        └── dogscats
            ├── sample
            │   ├── train
            │   │   ├── cats
            │   │   └── dogs
            │   └── valid
            │       ├── cats
            │       └── dogs
            ├── train
            │   ├── cats
            │   └── dogs
            └── valid
                ├── cats
                └── dogs
    """

    url = "http://files.fast.ai/data/dogscats.zip"
    filename = "dogscats.zip"
    checksum = "aef22ec7d472dd60e8ee79eecc19f131"

    def __init__(
        self,
        train_val: str,
        transform: Callable = None,
        target_transform: Callable = None,
        loader=default_loader,
        *,
        root: str = None,
        download: bool = False,
    ) -> None:
        _valid_train_val_values = ["train", "valid", "sample/train", "sample/valid"]
        if train_val not in _valid_train_val_values:
            print(
                f"`train_val` must be f'{_valid_train_val_values}' or 'valid'; is {train_val}"
            )

        if root is None:
            root = "~/data"
        self.root = Path(root).expanduser()

        if download:
            self._download()
            self._extract()

        if not self._check_integrity():
            raise RuntimeError(
                "Dataset not found or corrupted. "
                "You can use download=True to download it."
            )

        path = self.root / "dogscats" / train_val
        print(f"Loading data from {path}.")
        assert path.is_dir(), f"'{train_val}' is not valid."

        super().__init__(path, transform, target_transform, loader)

    def _download(self):
        if self._check_integrity():
            print("Dataset already downloaded and verified.")
            return

        root = self.root
        download_url(self.url, root, self.filename, self.checksum)

    def _extract(self):
        path_to_zip = self.root / self.filename
        with zipfile.ZipFile(path_to_zip, "r") as zip_ref:
            zip_ref.extractall(self.root)

    def _check_integrity(self):
        path_to_zip = self.root / self.filename
        return check_integrity(path_to_zip, self.checksum)


########################################################################################
# Misc
@dataclass
class OMeter:
    name: str = ""
    values: List[float] = field(default_factory=list)
    samples: List[float] = field(default_factory=list)

    def update(self, value, weight=1):
        self.values.append(value)
        self.samples.append(weight)

    def mean(self):
        return np.average(self.values, weights=self.samples)

    def __str__(self):
        return f"{(self.name or 'OMeter')}(mean={self.mean()}, n={sum(self.samples)})"


########################################################################################
if __name__ == "__main__":
    print("===============")
    print(" PYTORCH CHECK")
    print("===============")
    print(" * GPU count:  ", torch.cuda.device_count())
    print(" * GPU or CPU: ", DEVICE)
    print(" * CPU count:  ", CPU_COUNT)
