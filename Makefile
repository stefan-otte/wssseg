.PHONY: \
	clean \
	black \
	data \
	download_data \
	env_create \
	env_export \
	env_update \
	hooks \
	lint \
	ptw \
	sync_data_from_cloud \
	sync_data_to_cloud \
	test \


#################################################################################
# GLOBALS                                                                       #
#################################################################################

PROJECT_DIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
BUCKET = [OPTIONAL] your-bucket-for-syncing-data (do not include 'gs://')
PROJECT_NAME = wssseg

# convert ipynb -> html
NB_IPYNB = $(wildcard notebooks/*.ipynb)
NB_HTML = $(patsubst notebooks/%.ipynb,notebooks/%.html,$(NB_IPYNB))


#################################################################################
# COMMANDS                                                                      #
#################################################################################

## Make Dataset
data: download_data
	python wssseg/cli/preprocess.py

## Download the data
download_data: data/raw/ground_truth data/raw/dsm
	# Help: to download the data from the FTP server make sure the env vars ISPRS_USER and ISPRS_PW are set.
	@echo "ISPRS_USER: ${ISPRS_USER}"
	@echo "ISPRS_PW:   ${ISPRS_PW}"


## Delete all compiled Python files
clean:
	find . -type f -name "*.py[co]" -delete
	find . -type d -name "__pycache__" -delete


## Lint using flake8
lint:
	flake8 wssseg

## Autoformat with black
black:
	balck wssseg tests

## Run unittests
test:
	pytest

## Rerun unittest on every file change
ptw:
	ptw

## Upload Data into the cloud
sync_data_to_cloud:
	gsutil -m rsync -r data gs://$(BUCKET)/data/

## Download Data from the cloud
sync_data_from_cloud:
	gsutil -m rsync -r gs://$(BUCKET)/data/ data

## Create a conda env from the "environment.yml"
env_create:
	conda env create -f environment.yml

## Update the conda env from the "environment.yml"
env_update:
	conda env update -f environment.yml

## Export the current conda env to "environment.yml"
env_export:
	conda env export | grep -v "^prefix: " > environment.yml


#################################################################################
# PROJECT RULES                                                                 #
#################################################################################

notebooks/%.html: notebooks/%.ipynb
	jupyter-nbconvert --to html $<


data/raw/dsm:
	cd data/raw \
		&& wget ftp://${ISPRS_USER}:${ISPRS_PW}@ftp.ipi.uni-hannover.de/ISPRS_BENCHMARK_DATASETS/Vaihingen/ISPRS_semantic_labeling_Vaihingen.zip \
		&& unzip ISPRS_semantic_labeling_Vaihingen.zip

data/raw/ground_truth:
	cd data/raw \
		&& wget ftp://${ISPRS_USER}:${ISPRS_PW}@ftp.ipi.uni-hannover.de/ISPRS_BENCHMARK_DATASETS/Vaihingen/ISPRS_semantic_labeling_Vaihingen_ground_truth_COMPLETE.zip \
		&& unzip ISPRS_semantic_labeling_Vaihingen_ground_truth_COMPLETE.zip -d ground_truth



#################################################################################
# Self Documenting Commands                                                     #
#################################################################################

.DEFAULT_GOAL := show-help

# Inspired by <http://marmelab.com/blog/2016/02/29/auto-documented-makefile.html>
# sed script explained:
# /^##/:
# 	* save line in hold space
# 	* purge line
# 	* Loop:
# 		* append newline + line to hold space
# 		* go to next line
# 		* if line starts with doc comment, strip comment character off and loop
# 	* remove target prerequisites
# 	* append hold space (+ newline) to line
# 	* replace newline plus comments by `---`
# 	* print line
# Separate expressions are necessary because labels cannot be delimited by
# semicolon; see <http://stackoverflow.com/a/11799865/1968>
.PHONY: show-help
show-help:
	@echo "$$(tput bold)Available rules:$$(tput sgr0)"
	@echo
	@sed -n -e "/^## / { \
		h; \
		s/.*//; \
		:doc" \
		-e "H; \
		n; \
		s/^## //; \
		t doc" \
		-e "s/:.*//; \
		G; \
		s/\\n## /---/; \
		s/\\n/ /g; \
		p; \
	}" ${MAKEFILE_LIST} \
	| LC_ALL='C' sort --ignore-case \
	| awk -F '---' \
		-v ncol=$$(tput cols) \
		-v indent=19 \
		-v col_on="$$(tput setaf 6)" \
		-v col_off="$$(tput sgr0)" \
	'{ \
		printf "%s%*s%s ", col_on, -indent, $$1, col_off; \
		n = split($$2, words, " "); \
		line_length = ncol - indent; \
		for (i = 1; i <= n; i++) { \
			line_length -= length(words[i]) + 1; \
			if (line_length <= 0) { \
				line_length = ncol - indent - length(words[i]) - 1; \
				printf "\n%*s ", -indent, " "; \
			} \
			printf "%s ", words[i]; \
		} \
		printf "\n"; \
	}' \
	| more $(shell test $(shell uname) = Darwin && echo '--no-init --raw-control-chars')
