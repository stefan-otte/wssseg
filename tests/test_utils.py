import re

from wssseg.utils import lazy_property, now_str


########################################################################################
def test_now_str():
    assert re.match(r"^\d\d\d\d-\d\d-\d\d_\d\d:\d\d:\d\d$", now_str())


########################################################################################
def test_lazy_property():
    class LazyFoo:
        @lazy_property
        def bar(self):
            print("time consuming calculation")
            return "bar"

    foo = LazyFoo()
    assert not hasattr(foo, "_lazy_bar")
    assert foo.bar == "bar"
    assert hasattr(foo, "_lazy_bar")
