import pytest
import torch
import torch.nn as nn
import torch.nn.functional as F
from hypothesis import given
from hypothesis import strategies as st

from wssseg.torch_utils import (
    all_frozen,
    all_trainable,
    cos_sim,
    freeze_all,
    get_trainable,
    unfreeze_all,
)


def test_freezing_and_unfreezing_functions():
    model = nn.Sequential(*[nn.Linear(2, 5)])
    assert all_trainable(model.parameters())
    assert not all_frozen(model.parameters())
    assert len(list(get_trainable(model.parameters()))) > 0

    freeze_all(model.parameters())
    assert not all_trainable(model.parameters())
    assert all_frozen(model.parameters())
    assert len(list(get_trainable(model.parameters()))) == 0

    unfreeze_all(model.parameters())
    assert all_trainable(model.parameters())
    assert not all_frozen(model.parameters())
    assert len(list(get_trainable(model.parameters()))) > 0


########################################################################################
# cos_sin
@given(
    n=st.integers(min_value=1, max_value=10),
    m=st.integers(min_value=1, max_value=11),
    d=st.integers(min_value=1, max_value=15),
)
def test_cos_sim(n, m, d):
    A = torch.rand(n, d)
    B = torch.rand(m, d)

    sim = cos_sim(A, B)
    assert sim.shape == (n, m)

    # check against naive (but slow) implementation
    sim_ = torch.stack([F.cosine_similarity(a.unsqueeze(0), B) for a in A])
    assert torch.allclose(sim, sim_)


@given(
    n=st.integers(min_value=1, max_value=3),
    m=st.integers(min_value=1, max_value=4),
    d=st.integers(min_value=1, max_value=5),
)
def test_cos_sim_with_zero(n, m, d):
    A = torch.zeros(n, d)
    B = torch.zeros(m, d)
    sim = cos_sim(A, B)
    assert sim.shape == (n, m)
    assert not torch.any(torch.isnan(sim))


@pytest.mark.parametrize("a", [torch.ones(3), torch.ones(3, 2, 2)])
def test_cos_sim_with_not_2d_tensor(a):
    correct_tensor = torch.ones(2, 2)
    with pytest.raises(ValueError):
        cos_sim(a, a)
    with pytest.raises(ValueError):
        cos_sim(a, correct_tensor)
    with pytest.raises(ValueError):
        cos_sim(correct_tensor, a)
